output "external-ip" {
  description = "External IPv4 address"
  value       = vultr_instance.web.main_ip
}

output "external-ipv6" {
  description = "External IPv6 address"
  value       = vultr_instance.web.v6_main_ip
}
