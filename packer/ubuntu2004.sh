#!/bin/bash -x
####################################
## Build Ubuntu 20.04 with Covenant 
####################################

## Update the server.
echo "Initial update for system."
sudo apt update && sudo apt upgrade -y

# Core
echo "Install core utilities."
sudo apt install git tmux openvpn -y

# Docker
echo "Add docker repositories and install latest docker."
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
sudo apt install -y docker-ce docker-ce-cli containerd.io 

# .net Core
echo "Add .net core repositories and install latest .net."
wget https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
rm packages-microsoft-prod.deb
sudo apt-get update
sudo apt-get install -y apt-transport-https
sudo apt-get update
sudo apt-get install -y aspnetcore-runtime-5.0

# Covenant
echo "Pulling latest Covenant and creating fresh docker container."
git clone --recurse-submodules https://github.com/dazvid/Covenant
cd Covenant/Covenant
docker build -t covenant .
cd
echo -e '#!/bin/bash\n\ndocker run -it --name covenant -v /root/Covenant/Covenant/Data:/app/Data -p 80:80 -p 443:443 -p 7443:7443 covenant' > start.sh
chmod +x cov.sh

## Wipe unused disk space with zeros for security and compression.
echo "Clearing free disk space. This may take several minutes."
dd if=/dev/zero of=/zerofile status=progress
sync
rm /zerofile
sync
echo "System setup is complete. Begin snapshot process."
