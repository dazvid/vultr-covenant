variable "vultr_api_key" {
  type      = string
  default   = "${env("VULTR_API_KEY")}"
  sensitive = true
}

packer {
  required_plugins {
    vultr = {
      version = ">=v2.3.2"
      source  = "github.com/vultr/vultr"
    }
  }
}

source "vultr" "ubuntu2004" {
  api_key              = "${var.vultr_api_key}"
  os_id                = "387"
  plan_id              = "vc2-1c-1gb"
  region_id            = "syd"
  snapshot_description = "Ubuntu 20.04 ${formatdate("YYYY-MM-DD hh:mm", timestamp())}"
  ssh_username         = "root"
  state_timeout        = "25m"
  ssh_key_ids          = ["521add5e-c6ea-4351-b180-b59cdee6ce11"]
}

build {
  sources = ["source.vultr.ubuntu2004"]

  provisioner "shell" {
    script = "ubuntu2004.sh"
  }
}