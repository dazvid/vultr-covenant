terraform {
  required_providers {
    vultr = {
      source  = "vultr/vultr"
      version = "2.4.2"
    }
  }
}

# Configure the Vultr Provider
provider "vultr" {
  api_key = var.VULTR_API_KEY
}

# Create a web instance
resource "vultr_instance" "web" {
  snapshot_id      = var.snapshot-id
  plan             = "vc2-1c-1gb" # vultr-cli plan list
  region           = "syd"        # vultr-cli region list
  enable_ipv6      = true
  hostname         = "covenant"
  label            = "covenant"
  tag              = "covenant"
  activation_email = false
}

