variable "VULTR_API_KEY" {
  description = "Vultr API access token."
  type        = string
}

variable "ssh-keys" {
  description = "List of ssh public keys that will be added into authorized_hosts."
  type        = list(string)
  default     = ["521add5e-c6ea-4351-b180-b59cdee6ce11"] # vultr-cli ssh-key list
}

variable "snapshot-id" {
  description = "Vultr snapshot ID - can be acquired from `vultr-cli snapshot list`."
  type        = string
  default     = "181322af-8c82-4dad-98bb-9007c662e817" # vultr-cli snapshot list
}
