# What is this?

`Packer` creates a `snapshot` of a virtual machine with a little bit of provisioning.

`Terraform` is then used to create a VPS based from that `snapshot`.

# How do I use it?

1. Install packer and terraform (google is your friend)
2. Do this stuff:

```
git clone https://gitlab.com/dazvid/vultr-covenant
cd vultr-covenant
# You have to enable API access to your vultr account first, then copy your token
export TF_VAR_VULTR_API_KEY=<your vultr api key>          # magic name for terraform variables
export VULTR_API_KEY=<your vultr api key>                 # this env is used for the vultr-cli and packer
cd packer && packer init . && packer build .              # will take ~20 mins
cd .. && terraform init && terraform apply -auto-approve  # deploy!
```

3. ssh to the outputted IPv4 address from terraform (`terraform output`)
4. Run the `./start.sh` script to kick off Covenant in a docker container.
5. Browse to `https://<your VPS ip>:7443` and do a Covenant.

# Where do the magic numbers come from?

TL;DR: The Vultr API.

Use the [vultr-cli](https://github.com/vultr/govultr) to interrogate the current magic values.

```bash
export VULTR_API_KEY=<your api key here>
vultr-cli os list
vultr-cli region list
vultr-cli plan list
vultr-cli snapshot list
vultr-cli ssh-key list
# ...you get the idea
```
